module.exports = {
  isLoggedIn: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    } else {
      return res.redirect("/login-portal");
    }
  },
  isSuperAdmin: function(req, res, next) {
    if (req.isAuthenticated() && req.user.Role == "SUPER-ADMIN") {
      return next();
    } else {
      return res.redirect("/login-portal");
    }
  }
};
