
var LocalStrategy = require("passport-local").Strategy;
var crypto = require("crypto");

module.exports = function(passport, pool) {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "username",
        passwordField: "password"
      },
      function(username, password, done) {
        pool.query(
          "SELECT * FROM `reviewer` WHERE `Username` = '" + username + "'",
          function(err, rows) {
            if (err) return done(err);
            if (!rows.length) {
              return done(null, false, { message: "not found" }); // req.flash is the way to set flashdata using connect-flash
            }

            // if the user is found but the password is wrong
            if (
              !(
                rows[0].Password ==
                crypto
                  .createHash("md5")
                  .update(password.toString())
                  .digest("hex")
              )
            ) {
              return done(null, false, { message: "Wrong Password" });
            } // create the loginMessage and save it to session as flashdata
            // all is well, return successful user
            return done(null, rows[0]);
          }
        );
      }
    )
  );
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  // used to deserialize the user
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });
};
