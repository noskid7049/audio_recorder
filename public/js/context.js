var phrase_doc = document.getElementById("phrase");
var recording = false;

var micButton = document.getElementById("mic-recorder");
var micPath = document.getElementById("mic-path");
var select_gender = $("#gender-select").val();
var select_age = $("#age-select").val();
console.log(select_age);
console.log(select_gender);
window.onload = function() {
  var username = "";
  username += "J" + user.user_id;
  if (user_add_details.gender == "m") {
    username += "_ma";
  } else {
    username += "_fe";
  }

  if (user_add_details.device_type == "mobile") {
    username += "_mb";
  } else if (user_add_details.device_type == "pc") {
    username += "_pc";
  } else if (user_add_details.device_type == "tablet") {
    username += "_tb";
  }
  username += "_" + user_add_details.device_name;
  // console.log(username);
  $("#user_name").text(username);
  phraseIndexCheck();

  micButton.addEventListener("click", function() {
    if (
      select_gender != "" &&
      select_age != "" &&
      select_gender == user_add_details.gender &&
      select_age == user_add_details.age
    ) {
      if (recording) {
        recording = false;
        micButton.classList.remove("bg-red");
        micPath.classList.remove("fill-white");
        stopRecording();
      } else {
        recording = true;
        micButton.classList.add("bg-red");
        micPath.classList.add("fill-white");
        startRecording();
      }
    }

    // document.getElementById("phrase_container").classList.toggle("d-none");
    // document
    //   .getElementById("phrase_notice_container")
    //   .classList.toggle("d-none");
  });
};

function increasePhraseIndex() {
  phraseIndex++;
  phraseIndexCheck();
}
function phraseIndexCheck() {
  if (language_phrases.length == 0) {
    document.body.innerHTML = "";
    var p = document.createElement("p");
    p.appendChild(
      document.createTextNode(
        "There are no phrases for this project currently. Please try again later."
      )
    );
    document.body.appendChild(p);
    var final_btn = document.createElement("button");
    final_btn.setAttribute(
      "onclick",
      "window.location.href = 'https://m.me/theaudiobee'"
    );
    final_btn.innerHTML = '<i class="fab fa-facebook-messenger"></i>';
    final_btn.setAttribute("class", "btn btn-secondary");
    final_btn.style.backgroundColor = "#1F7EFC";
    final_btn.style.color = "#fff";
    final_btn.appendChild(document.createTextNode("Continue to Messenger"));
    document.body.append(final_btn);
  }
  if (phraseIndex >= language_phrases.length || phraseIndex >= 10) {
    // phrase.innerHTML = "You've completed the sample.";
    // micButton.style.display = "none";
    document.body.innerHTML = "";
    var p = document.createElement("p");
    p.appendChild(
      document.createTextNode(
        "The audio sample has been submitted. Please allow us 2-3 days to review the completed audio sample by the QA Team. Will get back to you with the feedback from the QA Team on the progress of the project."
      )
    );
    document.body.appendChild(p);
    var final_btn = document.createElement("button");
    final_btn.setAttribute(
      "onclick",
      "window.location.href = 'https://m.me/theaudiobee'"
    );
    final_btn.innerHTML = '<i class="fab fa-facebook-messenger"></i>';
    final_btn.setAttribute("class", "btn btn-secondary");
    final_btn.style.backgroundColor = "#1F7EFC";
    final_btn.style.color = "#fff";
    final_btn.appendChild(document.createTextNode("Continue to Messenger"));
    document.body.append(final_btn);
  } else {
    var current_phrase = language_phrases[phraseIndex];
    for (i = 0; i < completed_phrases.length; i++) {
      if (completed_phrases[i].Phrase_id == current_phrase.Phrase_id) {
        phraseIndex++;
        phraseIndexCheck();
      }
    }
    phrase.innerHTML = language_phrases[phraseIndex].Phrase;
    var language_id = language_phrases[phraseIndex].Language_id;
    // var req_language;
    language_info.forEach(lang => {
      if (lang.Language_id == language_id) {
        req_language = lang.Language_name;
      }
    });
    // document.getElementById("language_header").innerHTML =
    //   req_language + " Sample Audio Recording";
    document.getElementById("err").innerHTML = "";
  }
  // console.log("aud");
  // $("#audio_recorder_count").text((phraseIndex) + " / " + language_phrases.length);
}

$(".form-control").on("change", function() {
  select_gender = $("#gender-select").val();
  select_age = $("#age-select").val();
  if (
    select_gender != "" &&
    select_age != "" &&
    select_gender == user_add_details.gender &&
    select_age == user_add_details.age
  ) {
    $("#mic-recorder").removeClass("disabled");
  }
  else{
    $("#mic-recorder").addClass("disabled");
  }
});
