loaded = 0;
$(document).ready(function() {});
// Audio preloader with audio indexing

var non_native_count = 0;
var user_id;
var phrase_id;
// When the wave surfer is ready to play
wavesurfer.on("ready", function() {
  wavesurfer.play();
  // for straight line in the middle of the wave
  var c = document.getElementsByTagName("canvas")[1];
  var height = c.height;
  var width = c.width;
  var ctx = c.getContext("2d");
  ctx.beginPath();
  ctx.moveTo(0, height / 2);
  ctx.lineTo(width, height / 2);
  ctx.lineWidth = 0.5;
  ctx.stroke();
});

$(document).keypress(function(event) {
  // Check if the error modal is open
  if (!$("body").hasClass("modal-open")) {
    var event_code = event.which;
    if (event_code == 32 && !$("#btn-play_pause_toggle").is(":focus")) {
      wavesurfer.playPause();
    } else if (event_code == 97 || event_code == 65) {
      wavesurfer.skipBackward();
    } else if (event_code == 100 || event_code == 103) {
      wavesurfer.skipForward();
    }
  }
});
// Button events
$("#btn-play_pause_toggle").click(function() {
  wavesurfer.playPause();
});
$("#btn-backward").click(function() {
  wavesurfer.skipBackward();
});
$("#btn-forward").click(function() {
  wavesurfer.skipForward();
});
// Show text area in error modal if only checked
$(document).on("click", "#error_select input", function(event) {
  var req_textarea = "#errtxt_" + event.target.dataset.errorId;
  // console.log(req_textarea);
  if (event.target.checked) {
    $(req_textarea).css("display", "block");
  } else {
    $(req_textarea).css("display", "none");
  }
});
// Events for when next button is pressed
$("#btn-next").click(function() {
  var error_list = [];
  // Return all the error selected
  $("#error_select input:checked").each(function() {
    console.log($(this)[0].labels[0].innerText.indexOf("Non-native"));
    if ($(this)[0].labels[0].innerText.indexOf("Non-native") >= 0) {
      console.log("non-native");
      non_native_count++;
    }
    var error =
      '{ "error_id":' +
      $(this).attr("data-error-id") +
      ',"error_comment":"' +
      $("#errtxt_" + $(this).attr("data-error-id")).val() +
      '"}';
    error_list.push(error);
  });
  // Reset the error field in the error modal
  $("#error_select input").prop("checked", false);
  $("#error_select textarea").val("");
  $("#error_select textarea").css("display", "none");
  // For error logging in the db
  var split = file_name[audio_index].split("_");
  user_id = split[0];
  phrase_id = split[1].slice(0, -4);
  $.ajax({
    url: "/api/set-error-comments",
    type: "POST",
    contentType: "application/json",
    dataType: "json",
    data: JSON.stringify({
      error: error_list,
      user_id: user_id,
      phrase_id: phrase_id
    }),
    success: function(result) {
      console.log("OK");
    }
  });
  if (error_list.length > 0) {
    error_count++;
  }
  error_list = [];
  // Load the next audio
  audio_index++;
  var split = file_name[audio_index - 1].split("_");
  user_id = split[0];
  if (!approve_shown) {
    if (audio_index >= 2 && error_count == 0) {
      $("#btn-approve").css("display", "block");
      approve_shown = true;
    } else if (audio_index >= 4 && error_count == 1) {
      $("#btn-approve").css("display", "block");
      approve_shown = true;
    } else if (audio_index >= 6 && error_count == 2) {
      $("#btn-approve").css("display", "block");
      approve_shown = true;
    } else if (audio_index >= 9 && error_count == 3) {
      $("#btn-approve").css("display", "block");
      approve_shown = true;
    }
  }
  if (non_native_count >= 3) {
    $("#btn-send-redo").css("display", "block");
  }

  if (audio_index < file.length) {
    wavesurfer.empty();
    wavesurfer.load(file[audio_index]);
    $("#phrase").text(phrases_list[audio_index]);
  } else {
    wavesurfer.empty();
    $.ajax({
      url: "./api/set-current-user-status",
      method: "POST",
      data: {
        status: 5,
        user_id: user_id,
        phrase_id: phrase_id
      },
      success: function() {
        console.log("Off");
        if (typeof reviewer_review != "undefined") {
          if (reviewer_review == true) {
            // $.ajax({
            //   url: "./api/set-reviewer-review-details",
            //   method: "POST",
            //   data: {
            //     user_id: user_id
            //   },
            //   success: function() {
            //     window.location.reload();
            //   }
            // });
          }
        }
        window.location.reload(true);
      }
    });
  }
  if (typeof reviewer_review != "undefined") {
    console.log("herer");
    if (reviewer_review) {
      check_user_errors();
    }
  }
});
$("#btn-approve").on("click", function() {
  console.log("approve");
  $.ajax({
    url: "./api/set-current-user-status",
    method: "POST",
    data: {
      status: 4,
      user_id: user_id,
      phrase_id: phrase_id
    },
    success: function() {
      console.log("off");
      status_updated = true;
      window.location.reload(true);
    }
  });
});
$("#btn-send-redo").on("click", function() {
  console.log("Redo");
  $.ajax({
    url: "./api/set-current-user-status",
    method: "POST",
    data: {
      status: 5,
      user_id: user_id,
      phrase_id: phrase_id
    },
    success: function() {
      console.log("off");
      status_updated = true;
      window.location.reload(true);
    }
  });
});
// var valid_count = 0;
var error_count = 0;
var approve_shown = false;
