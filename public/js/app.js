var gumStream; //stream from getUserMedia()
var rec; //Recorder.js object
var input; //MediaStreamAudioSourceNode we'll be recording
// shim for AudioContext when it's not avb.
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext; //audio context to help us record

function startRecording() {
  var constraints = { audio: true, video: false };
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(function(stream) {
      audioContext = new AudioContext();
      //update the format
      // document.getElementById("formats").innerHTML =
      //   "Format: 1 channel pcm @ " + audioContext.sampleRate / 1000 + "kHz";
      /*  assign to gumStream for later use  */
      gumStream = stream;
      /* use the stream */
      input = audioContext.createMediaStreamSource(stream);
      rec = new Recorder(input, { numChannels: 1 });
      //start the recording process
      rec.record();
    })
    .catch(function(err) {});
}
function stopRecording() {
  rec.stop();

  //stop microphone access
  gumStream.getAudioTracks()[0].stop();
  rec.exportWAV(createDownloadLink);
}
function createDownloadLink(blob) {
  var url = URL.createObjectURL(blob);
  var au = document.createElement("audio");
  var li = document.createElement("li");
  li.setAttribute("id", "phrase_" + phraseIndex);
  li.setAttribute("class", "recordings row");

  //name of .wav file to use during upload and download (without extendion)

  var myaudio = new Audio(url);

  var div = document.createElement("div");
  div.setAttribute("class", "col-11");
  div.innerText = "File #" + (phraseIndex + 1) + " Uploading.";
  li.appendChild(div);

  var div2 = document.createElement("div");

  div2.id = "div_" + phraseIndex;
  div2.setAttribute("class", "audio-trigger");
  div2.innerHTML = '<i class="fa fa-play-circle"></i>';
  li.appendChild(div2);

  if (parseInt(phraseIndex) > 0) {
    var req = "#div_" + (phraseIndex - 1);
    $(req).css("display", "none");
  }
  $(document).on("click", ".audio-trigger", function() {
    myaudio.play();
  });
  //upload link
  var upload = document.createElement("a");
  upload.href = "#";
  upload.innerHTML = "Upload";
  upload.addEventListener("click", function(event) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (xhr.response == "Success") {
          console.log(xhr.response);
          div.innerText = "File #" + phraseIndex + " Upload Successful.";
        }
      }
    };
    var fd = new FormData();
    fd.append("file", blob, language_phrases[phraseIndex].Phrase_id);
    fd.append("language_id", language_phrases[phraseIndex].Language_id);
    fd.append("phrase_index", phraseIndex);

    // console.log("Hello");
    xhr.open("POST", "/api/upload-audio", true);
    xhr.send(fd);
  });
  upload.click();

  //add the li element to the ol
  recordingsList.prepend(li);
  increasePhraseIndex();
}
// $(document).on("click", "#div_" + phraseIndex, function() {
//   myaudio.play();
// });
