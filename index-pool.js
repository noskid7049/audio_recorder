const express = require("express");
var session = require("express-session");
var cors = require("cors");
var passport = require("passport");
var bodyparser = require("body-parser");
const flash = require("connect-flash");

const app = express();

app.set("view engine", "ejs");
app.set("views", __dirname + "/public/views");
app.use(express.static(__dirname + "/public")); //For serving the static files like css and js

app.use(cors());
app.use(
  session({ secret: "WmXgIlMOFQ", resave: true, saveUninitialized: true })
);
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(flash());

// Database Pool
const pool = require("./config/pool");

// Passport Config
require("./config/passport")(passport, pool);

// Passport Session
app.use(passport.initialize());
app.use(passport.session());

// Routes

app.use("/", require("./routes/index"));
app.use("/api", require("./routes/api"));

app.listen(3000, function() {
  console.log("Listening on port 3000!");
});
