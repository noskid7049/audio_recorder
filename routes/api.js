const express = require("express");
var formidable = require("formidable");
var crypto = require("crypto");
var fs = require("fs");
var GoogleSpreadsheet = require("google-spreadsheet");
var async = require("async");
var axios = require("axios");
const path = require("path");

const router = express.Router();

// Database Pool
const pool = require("../config/pool");

// AWS configu
var AWS = require("aws-sdk");
AWS.config.loadFromPath(path.resolve(__dirname, "../config/config.json"));
var s3 = new AWS.S3();

// Create a bucket and upload something into it
var bucketName = "audio-bee-audio-recording";
// Google Sheets
var doc = new GoogleSpreadsheet("14ib_BUdO2HGTxt923Rd29rP0Be8e90vqQm0uytOPAeY");
var creds = require("../config/audiobee-webapp-service-account.json");

function updateApprovedList(lang_id, user_id) {
  var language_name, user_details;
  var req_sheet;
  var sql = `SELECT * FROM languages where Language_id = ${lang_id}`;
  var headers = [
    "date",
    "name",
    "psid",
    "gender",
    "device",
    "devicename",
    "osused",
    "payment",
    "email"
  ];
  pool.query(sql, (err, result) => {
    if (err) throw err;
    language_name = result[0].Language_name;
    var sql = `SELECT * from user where User_id = ${user_id}`;
    pool.query(sql, (err, result) => {
      if (err) throw err;
      user_details = result[0];
      var today = new Date();
      var date =
        today.getMonth() +
        1 +
        "/" +
        today.getDate() +
        "/" +
        today.getFullYear();
      var payload = {
        date: date,
        name: "",
        psid: user_details.PSID.toString(),
        gender: user_details.Gender,
        device: user_details.Device_type,
        devicename: user_details.Device_name,
        osused: user_details.OS_Used,
        payment: user_details.Payment_option,
        email: user_details.Payment_account
      };
      doc.useServiceAccountAuth(creds, function() {
        doc.getInfo((err, info) => {
          info.worksheets.forEach((worksheet, index) => {
            if (worksheet.title == language_name) {
              req_sheet = index + 1;
            }
          });

          if (typeof req_sheet == "undefined") {
            doc.addWorksheet(
              { title: language_name, headers: headers },
              (err, sheet) => {
                sheet.addRow(payload, (err, row) => {});
              }
            );
          } else {
            doc.addRow(req_sheet, payload, (err, row) => {});
          }
        });
      });
    });
  });
}

// Return the terms from the db
router.post("/get-agreement", (req, res) => {
  var sql =
    "SELECT * FROM language_term WHERE Language_id = " +
    req.body.language +
    " LIMIT 1";
  pool.query(sql, function(err, result) {
    if (err) throw err;
    res.send(result[0]);
  });
});

// For uploading the audio file to AWS S3
router.post("/upload-audio", (req, res) => {
  if (req.session.user_id) {
    new formidable.IncomingForm().parse(req, (err, fields, files) => {
      if (err) throw err;
      var audio_number = files.file.name;
      var key =
        crypto
          .createHash("md5")
          .update(req.session.user_id.toString())
          .digest("hex") +
        "_" +
        audio_number +
        ".wav";
      // read file for upload
      fs.readFile(files.file.path, (err, data) => {
        if (err) throw err;
        // Upload to S3 server
        s3.putObject(
          {
            Bucket: bucketName,
            Key: key,
            Body: data,
            ACL: "public-read"
          },
          function(err, data) {
            if (err) console.log(err);
            console.log(
              "Successfully uploaded data to " + bucketName + "/" + key
            );
            var sql =
              "UPDATE user_info SET Audio_Count = Audio_Count + 1 WHERE User_id = '" +
              req.session.user_id +
              "' and Language_id = " +
              fields.language_id;
            pool.query(sql, function(err, res) {
              // Update the session audio count for the user
              if (err) throw err;
            });

            // SQL for updating the user phrase table
            sql =
              "INSERT INTO user_phrase VALUES (" +
              req.session.user_id +
              "," +
              audio_number +
              ")";
            pool.query(sql, (err, res) => {
              if (err) throw err;
            });
            // Beginning of the recordings
            if (parseInt(fields.phrase_index) == 0) {
              req.session.status = 1;
              var sql =
                "UPDATE user_info SET Status = 1 WHERE User_id = '" +
                req.session.user_id +
                "' and Language_id = " +
                fields.language_id;

              // SQL updating status of the user to completed/review remaining
              pool.query(sql, function(err, res) {
                if (err) throw err;
              });
            }
            // End of the recordings
            if (parseInt(fields.phrase_index) >= 9) {
              req.session.status = 2;
              var sql =
                "UPDATE user_info SET Status = 2 WHERE User_id = '" +
                req.session.user_id +
                "' and Language_id = " +
                fields.language_id;
              // SQL updating status of the user to completed/review remaining
              pool.query(sql, function(err, res) {
                if (err) throw err;
              });
            }
            res.send("Success");
          }
        );
      });

      req.session.audio_count += 1;
      req.session.save();
    });
    // Sql for updating the done audio count
  }
});
router.post("/get-error-list", function(req, res) {
  var sql = "SELECT * FROM comments";
  pool.query(sql, function(err, result) {
    if (err) throw err;
    res.send(result);
  });
});

// Get all the status information for comments
router.post("/get-status-info", function(req, res) {
  var sql = "SELECT * FROM status_info";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  });
});

// Setting the error/comments for user related errors
router.post("/set-error-comments", function(req, res) {
  var s = req.body;
  var full_url = s.user_id.split("/");
  var inp_user_id = full_url[full_url.length - 1];
  var phrase_id = s.phrase_id;

  var sql =
    "SELECT * FROM language_phrases WHERE Phrase_id = " +
    phrase_id +
    " LIMIT 1";
  pool.query(sql, function(err, result) {
    if (err) throw err;
    var sql =
      "SELECT User_id,md5(User_id) hash from user_info where Language_id = " +
      result[0].Language_id;
    pool.query(sql, function(err, rows) {
      if (err) throw err;
      rows.forEach(function(row) {
        if (row.hash == inp_user_id) {
          var rev_id = req.session.user.Reviewer_id;
          var user_id = row.User_id;
          if (s.error.length == 0) {
            console.log("No error logged");
          } else {
            s.error.forEach(function(error) {
              var error_id = JSON.parse(error).error_id;
              var error_comment = JSON.parse(error).error_comment;
              var sql =
                "INSERT INTO phrase_comment (Reviewer_id, User_id,Phrase_id,Comment_id,Further_comments) VALUES (" +
                rev_id +
                ", " +
                user_id +
                ", " +
                phrase_id +
                ", " +
                error_id +
                ", '" +
                error_comment +
                "')";
              pool.query(sql, function(err, result) {
                if (err) throw err;
                console.log("Error logged successfully");
              });
            });
          }
        }
      });
    });
  });
  res.end('{"success" : "Updated Successfully", "status" : 200}');
});
router.post("/set-current-user-status", function(req, res) {
  var s = req.body;
  var full_url = s.user_id.split("/");
  var inp_user_id = full_url[full_url.length - 1];
  var phrase_id = s.phrase_id;
  var sql =
    "SELECT * FROM language_phrases WHERE Phrase_id = " +
    phrase_id +
    " LIMIT 1";
  pool.query(sql, function(err, result) {
    if (err) throw err;
    var language_id = result[0].Language_id;

    var sql =
      "SELECT User_id,md5(User_id) hash from user_info where Language_id = " +
      language_id;
    pool.query(sql, function(err, rows) {
      if (err) throw err;
      rows.forEach(function(row) {
        if (row.hash == inp_user_id) {
          var rev_id = req.session.user.Reviewer_id;
          var user_id = row.User_id;
          var sql =
            "UPDATE user_info SET Status = " +
            req.body.status +
            " WHERE user_id = " +
            user_id +
            " AND Language_id = " +
            language_id;
          if (req.body.status == 4) {
            updateApprovedList(language_id, user_id);
          }
          pool.query(sql, function(err, result) {
            if (err) throw err;
            console.log("Update status: " + req.body.status);
            res.send("Success");
          });
        }
      });
    });
  });
});
router.post("/get-user-info", function(req, res) {});

router.post("/get-user-error-list", function(req, res) {
  var sql =
    "SELECT * FROM phrase_comment where User_id = " +
    req.body.recorded_user +
    " AND Phrase_id = " +
    req.body.phrase_id;
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  });
});

module.exports = router;
