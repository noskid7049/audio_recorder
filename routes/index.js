const express = require("express");
var crypto = require("crypto");
var passport = require("passport");
var url = require("url");
var Promise = require("promise");
var async = require("async");
var axios = require("axios");

const router = express.Router();

// Database Pool
const pool = require("../config/pool");

// Check Authentication
const { isLoggedIn, isSuperAdmin } = require("../config/auth");

router.get("/", (req, res) => {
  res.redirect("/completeprofile");
});
// Get the initial form
router.get("/completeprofile", (req, res) => {
  var languages;

  var sql = "SELECT * FROM languages ORDER BY Language_name";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    languages = result;
    var user_type;
    if (typeof req.query.user_type == "undefined") {
      user_type = "bot";
    } else {
      user_type = "web";
    }
    res.render("completeprofile", {
      psid: req.query.psid,
      user_type,
      languages: languages
    });
  });
});
// Set Initial form data to db
router.post("/set-user-profile", (req, res) => {
  var obj = req.body;

  req.session.user_details = obj;

  var sql =
    "INSERT INTO user (PSID,Device_type,Device_name, Gender,Age, Payment_option,Payment_account,Country,Payment_name,OS_Used,User_type) VALUES ('" +
    obj.userId +
    "','" +
    obj.device_type +
    "','" +
    obj.device_name +
    "','" +
    obj.gender +
    "','" +
    obj.age +
    "','" +
    obj.payment_option +
    "','" +
    obj.payment_account +
    "','" +
    obj.country +
    "','" +
    obj.payment_name +
    "','" +
    obj.os_used +
    "','" +
    obj.user_type +
    "')";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.redirect(
      url.format({
        pathname: "/update-user-lang-info",
        query: {
          user_id: result.insertId,
          languages: obj.languages,
          native_language: obj.native_language
        }
      })
    );
  });
});
// Set the lang data
router.get("/update-user-lang-info", (req, res) => {
  if (!req.session.user_id) {
    req.session.user_id = req.query.user_id;
    var languages = JSON.parse(req.query.languages);
    languages.forEach(function(language) {
      if (language != req.query.native_language) {
        var sql =
          "INSERT INTO user_lang (User_id, Language_id) VALUES (" +
          req.query.user_id +
          "," +
          language +
          ")";
      } else {
        var sql =
          "INSERT INTO user_lang (User_id, Language_id,Native) VALUES (" +
          req.query.user_id +
          "," +
          language +
          ",'T')";
      }
      var sql2 =
        "INSERT INTO user_info (User_id, Language_id) VALUES (" +
        req.query.user_id +
        "," +
        language +
        ")";
      pool.query(sql, (err, result) => {
        if (err) throw err;
        console.log("user " + req.query.user_id + " added to " + language);
      });
      pool.query(sql2, (err, result) => {
        if (err) throw err;
        console.log("user info added");
      });
    });
    req.session.languages = languages;
    req.session.native_language = req.query.native_language;
  }
  res.redirect("/user-agreement");
});

// Get the user agreement modal
router.get("/user-agreement", (req, res) => {
  var languages;
  var {user_id} = req.query;
  var sql = "SELECT * FROM languages ORDER BY Language_name";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    languages = result;
    res.render("user_agreement", {
      languages: languages
    });
  });
});
// Get the audio recorder env
router.get("/audio-recorder", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  // console.log(req.session.user_id);
  if (req.session.user_id) {
    var sql =
      "SELECT User_id,Language_id,Audio_Count,Status from user_info where User_id = " +
      req.session.user_id;
    pool.query(sql, (err, result) => {
      if (err) throw err;
      var user_info = result;
      var language_phrases;
      var language_info;
      var sql =
        "SELECT * FROM language_phrases WHERE Language_id in (" +
        query.language_id +
        ")";
      pool.query(sql, (err, result) => {
        if (err) throw err;
        language_phrases = result;
        var sql =
          "SELECT * FROM languages where Language_id in (" +
          query.language_id +
          ")";
        pool.query(sql, (err, result) => {
          if (err) throw err;
          language_info = result;
          var sql =
            "SELECT * FROM user_phrase WHERE User_id = " + req.session.user_id;
          pool.query(sql, (err, result) => {
            if (err) throw err;
            var completed_phrases = result;
            var params = {
              user_info: user_info,
              user_add_details: req.session.user_details,
              language_phrases: language_phrases,
              language_info: language_info,
              completed_phrases: completed_phrases
            };
            console.log(params);
            res.render("index", params);
          });
        });
      });
    });
  }
});

router.get("/admin/reviewer-signup-01191", (req, res) => {
  res.render("reviewer-signup");
});
router.post("/reviewer-signup", (req, res) => {
  var fields = req.body;
  var full_name = fields.full_name;
  var username = fields.uname;
  var password = crypto
    .createHash("md5")
    .update(fields.password.toString())
    .digest("hex");
  console.log("hello");
  var select_query =
    "SELECT * FROM reviewer where Username = '" + username + "'";
  pool.query(select_query, (err, row) => {
    if (err) throw err;
    if (row.length) {
      res.send("User already exists");
    } else {
      var sql =
        "INSERT INTO reviewer(Name, Username, Password) VALUES ('" +
        full_name +
        "','" +
        username +
        "','" +
        password +
        "')";
      pool.query(sql, (err, result) => {
        if (err) throw err;
        res.send("Reviewer signed");
      });
    }
  });
});

router.get("/login-portal", (req, res) => {
  res.render("login-portal");
});

router.post("/login-portal", function(req, res, next) {
  passport.authenticate("local", function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      // *** Display message without using flash option
      // re-render the login form with a message
      return res.send(info.message);
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      req.session.user = user;
      if (user.Role == "SUPER-ADMIN") {
        return res.redirect("/audio-status");
      }
      return res.redirect("/choose-reviewer-language");
    });
  })(req, res, next);
});
router.get("/choose-reviewer-language", isLoggedIn, function(req, res) {
  req.session.language_id = 54;
  var sql =
    "SELECT l.Language_id,l.Language_name,(SELECT COUNT(*) FROM user_info ui, user_lang ul WHERE ui.Language_id=l.Language_id and ui.User_id = ul.User_id and ui.Language_id=ul.Language_id and ul.Native = 'T' and STATUS=2) num_completed from languages l,reviewer_lang rl where rl.Language_id = l.Language_id and rl.Reviewer_id = " +
    req.session.user.Reviewer_id;
  sql += " ORDER BY l.Language_name";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.render("choose-reviewer-language", { table: result });
  });
});
// Reviewer portal
router.get("/reviewer-portal", isLoggedIn, async function(req, res) {
  var url_prefix =
    "https://audio-bee-audio-recording.s3.us-east-2.amazonaws.com/";
  var fileUrl = [];
  var phrases_list = [];
  var recorded_user, recorded_user_id;
  var req_psid = [];
  // Getting data where user data language of reviewer and user is same

  if (req.session.user.Role == "LEAD") {
    await axios
      .post(
        "https://bot.theaudiobee.com/api/v1/bots/delta-labs/mod/referral/get-referral",
        {
          limit: 50000
        }
      )
      .then(data => {
        data.data.referral.forEach(row => {
          if (row.referrer_id == req.session.user.PSID) {
            req_psid.push(row.user_id);
          }
        });
      });
  }
  function query() {
    return new Promise(function(resolve, reject) {
      // var language_id = req.query.language_id;
      // console.log(req.session.user);
      if (typeof req.query.req_language != "undefined") {
        var language_id = req.query.req_language;
        var sql =
          "SELECT ui.User_id User_id,ui.Language_id Language_id, u.PSID PSID FROM user_info ui, user_lang ul, user u WHERE ui.Language_id IN ('" +
          language_id +
          "') AND ui.User_id = ul.User_id and ui.Language_id = ul.Language_id and ui.User_id = u.User_id and ul.Native = 'T' and ui.Status = 2";
        if (req.session.user.Role == "LEAD") {
          sql += " and u.PSID in (" + req_psid.join() + ")";
        }
        sql += " LIMIT 1";
      } else if (typeof req.query.req_user_id != "undefined") {
        var user_id = req.query.req_user_id;
        recorded_user_id = user_id;
        // console.log(req.query);
        var sql =
          "SELECT ui.User_id User_id, ui.Language_id Language_id, u.PSID PSID from user_info ui,user u WHERE ui.User_id = " +
          user_id +
          " AND ui.User_id = u.User_id";
      } else {
        reject(false);
      }
      pool.query(sql, (err, result) => {
        if (err) {
          if (err.code == "ER_EMPTY_QUERY") {
            reject(false);
          } else {
            throw err;
          }
        } else {
          if (result.length > 0) {
            if (
              req.session.user.Role == "SUPER-ADMIN" ||
              req.session.user.Role == "LEAD"
            ) {
              recorded_user = result[0].PSID;
            }
            var key = crypto
              .createHash("md5")
              .update(result[0].User_id.toString())
              .digest("hex");
            var sql =
              "SELECT lp.Phrase_id,lp.Phrase,l.Language_name FROM `user_phrase` up, language_phrases lp,languages l WHERE User_id = " +
              result[0].User_id +
              " and up.Phrase_id = lp.Phrase_id and lp.Language_id = l.Language_id and l.Language_id = " +
              result[0].Language_id;
            pool.query(sql, (err, result) => {
              if (result.length == 0) {
                reject(false);
              }
              if (err) throw err;
              req.session.language = result[0].Language_name;
              result.forEach(function(row, index) {
                fileUrl.push(url_prefix + key + "_" + row.Phrase_id + ".wav");
                phrases_list.push(row.Phrase);
                if (index == result.length - 1) {
                  resolve(result);
                }
              });
            });
          } else {
            reject(false);
          }
        }
      });
    });
  }
  async.series([
    function() {
      query().then(
        function(row) {
          var payload = {
            user_add_details: req.session.user,
            language: req.session.language,
            file_list: fileUrl,
            phrases_list: phrases_list,
            recorded_user: recorded_user,
            recorded_user_id: recorded_user_id
          };
          res.render("reviewer-portal", payload);
        },
        function(row) {
          res.render("nomoresession");
        }
      );
    }
  ]);
});

// ***********************************************************************
//                             Super-Admin
// ***********************************************************************
router.get("/audio-status", isSuperAdmin, function(req, res) {
  var sql = `SELECT l.Language_name language,
              l.Language_id language_id,
              COUNT(*) total,
              COUNT(case
                   WHEN Status in (1)
                   THEN 1
                   ELSE NULL
                   END) inprogress,
              COUNT(case
                   WHEN Status in (2)
                   THEN 1
                   ELSE NULL
                   END) completed,
              COUNT(case
                   WHEN Status in (4)
                   THEN 1
                   ELSE NULL
                   END) approved
FROM user_info u, languages l,user_lang ul WHERE u.Language_id=l.Language_id and u.User_id = ul.User_id and u.Language_id = ul.Language_id and ul.Native = "T" GROUP by u.Language_id`;
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.render("audio-status", { table: result });
  });
});
router.get("/language-status", isSuperAdmin, (req, res) => {
  var sql =
    "SELECT * FROM user u, user_info ui, status_info si,user_lang ul where u.User_id = ui.User_id and u.User_id = ul.User_id and ul.Native = 'T' and ui.Status = si.status_id and ui.Language_id = " +
    req.query.language_id;

  sql += " ORDER BY ui.ID_Generated_Time DESC";
  pool.query(sql, (err, result) => {
    if (err) throw err;
    res.render("language-status", { table: result });
  });
});

// ************************************************************************
//                              Error Handling
// ************************************************************************
process.on("uncaughtException", err => {
  console.error(err, "Uncaught Exception thrown");
  process.exit(1);
});

router.get("/dev/num-free-connection", (req, res) => {
  console.log(pool._freeConnections.length);
  res.send("bye");
});

//
// ================================================================
// Test
// ==============================================================
//

router.get("/test/abgs-interview", (req, res) => {
  var url = [
    "https://www.youtube.com",
    "https://www.google.com",
    "https://www.facebook.com",
    "https://www.twitter.com"
  ];
  res.redirect(url[Math.floor(Math.random() * url.length)]);
});

module.exports = router;
